import os
import sys
import glob
import json
import random
import sqlite3
import math
import colorsys
from xml.etree import ElementTree as etree

from PIL import Image

"""
Generate a mosaic of images

Base image is a random image from the collection
Image average colors are precomputed and stored in a database

TODO:
* caching - save color analysis
* do not overwrite thumbnails unless specified
"""

def hsv_dist(c1, c2):
	c1 = map(lambda c: c/255.0, c1)
	c2 = map(lambda c: c/255.0, c2)
	color1hsv = colorsys.rgb_to_hsv(*c1)
	color2hsv = colorsys.rgb_to_hsv(*c2)


	total = abs((color1hsv[0]-color2hsv[0])%1)
	total += abs(color1hsv[1]-color2hsv[1])
	total += abs(color1hsv[2]-color2hsv[2])
	return total

def calculate_distance(color1, color2, config):
	"""
	euclidean distance between the two vectors
	"""
	if config["colormode"] == "HSV":
		return  hsv_dist(color1, color2)
	elif config["colormode"] == "L":
		return calculate_distance_mono(color1, color2)

	result = 0

	color1 = map(lambda c: c/255.0, color1)
	color2 = map(lambda c: c/255.0, color2)

	for band1, band2 in zip(color1, color2):
		result += (band1 - band2)**2
	result = result**0.5
	return result

def calculate_distance_mono(color1, color2):
	print color1, color2
	return abs((color1/255.0)-(color2/255.0))

def open_random_rgb_image(images, config):
	newImageName = random.choice(images)
	newImage = Image.open(newImageName)

	while len(newImage.getbands()) != 3:
		newImageName = random.choice(images)
		newImage = Image.open(newImageName)
	return newImage

def get_image_list(config):
	return glob.glob(config["glob_pattern"])

def find_images(baseImage, imageData, config):
	images = []
	print "Searching for matching images..."
	for pixel in baseImage.getdata():
		closest_image = find_closest_image(pixel, imageData, config)
		images.append(closest_image)
	print "found."
	return images

def find_closest_image(color, imageColors, config):
	"""
	* calculate distances to all image colors
	* pick closest one or one with a small error
	or
	* choose a random image color (monte carlo)
	* calculate distance
	* if within given limit, return image
	* the longer the image search, the higher the limit (more likely to choose an image)

	* if the search doesn't finish then return the last image?
	"""
	tolerance = config["start_tolerance"]
	imageData = random.choice(imageColors)
	dist = calculate_distance(color, imageData["avgColor"], config)

	while dist > tolerance:
		imageData = random.choice(imageColors)
		dist = calculate_distance(color, imageData["avgColor"], config)
		tolerance += config["tolerance_increment"]
	print "dist", dist
	print "tolerance", tolerance

	return imageData

def get_image_avg_color(img, config):
	"""
	* calculate average color by summing each band and dividing by # of images
	* get mode directly from histogram (easy)
	"""
	resize_image_longside(img, config["downsample_dimension"])

	# returns a tuple of (frequency, color)
	colorsData = img.getcolors(img.size[0]*img.size[1])
	# sort by frequency
	colorsData.sort(key=lambda col: col[0], reverse=True)
	# return most frequent (mode)
	# this may change
	return colorsData[0]

def write_image_colors(data, config):
	conn = sqlite3.connect(config["database"])
	cur = conn.cursor()
	# cur.execute("blabal")

def generate_svg(images, baseImage, config):
	print "Generating svg..."
	etree.register_namespace("xlink","http://www.w3.org/1999/xlink")
	etree.register_namespace("","http://www.w3.org/2000/svg")
	root = etree.XML('<svg version="1.1"></svg>')
	root.set("xmlns", "http://www.w3.org/2000/svg")
	root.set("xmlns:xlink", "http://www.w3.org/1999/xlink")

	for i, image in enumerate(images):
		img = Image.open(image["thumbnail_path"])
		tilesize = get_scaled_size_shortside(
			img.size,
			config["tile_base_size"]
		)

		imgEl = etree.SubElement(root, "image")
		imgEl.set("xlink:href", image["thumbnail_path"])
		imgEl.set("x", str((i%baseImage.size[0])*config["tile_base_size"]))
		imgEl.set("y", str((i/baseImage.size[0])*config["tile_base_size"]))
		imgEl.set("width", str(tilesize[0]))
		imgEl.set("height", str(tilesize[1]))

	tree = etree.ElementTree(root)
	print "done."
	return tree

def get_scaled_size_longside(size, side):
	# get the image size from uniform scaling to match the long side
	ratio = side/float(max(size))

	newSize = (
		math.floor(ratio*size[0]),
		math.floor(ratio*size[1])
	)

	return newSize

def get_scaled_size_shortside(size, side):
	# get the image size from uniform scaling to match the long side
	ratio = side/float(min(size))

	newSize = (
		math.floor(ratio*size[0]),
		math.floor(ratio*size[1])
	)

	return newSize

def resize_image_longside(image, side):
	newSize = get_scaled_size_longside(image.size, side)
	image.thumbnail(newSize)

def calculate_avg_colors(images, config):
	# put a cap for debugging
	print "Calculating average colors for all images...({})".format(len(images))
	colorList = []
	for imagePath in images:
		image = Image.open(imagePath)
		sys.stdout.write("=")
		sys.stdout.flush()
		# skip non-rgb images
		if config["colormode"] == "HSV" and \
		len(image.getbands()) != 3:
			continue
		elif config["colormode"] == "L":
			image = image.convert("L")

		avgColor = get_image_avg_color(image, config)
		colorList.append({
			"path" : imagePath,
			"avgColor" : avgColor[1],
		})
		# if len(colorList) >= num:
		# 	break
	print "done."
	return colorList

def set_thumbnails(imagePaths, config):
	print "Exporting thumbnails..."
	for imagePath in imagePaths:
		sys.stdout.write("=")
		sys.stdout.flush()
		thumbPath = os.path.join(config["thumbnail_path"],
			os.path.basename(imagePath["path"]))
		imagePath["thumbnail_path"] = thumbPath
		if "export_thumbnails" in config and \
		config["export_thumbnails"] is True:
			image = Image.open(imagePath["path"])

			if config["colormode"] == "L":
				image = image.convert("L")

			if not os.path.isfile(imagePath["thumbnail_path"]) or \
						config["overwrite_thumbnails"]:
				newSize = get_scaled_size_shortside(image.size, config["tile_base_size"])
				image.thumbnail(newSize)
				image.save(thumbPath)
	print "done."

if __name__ == "__main__":
	# generate a complete mosaic
	conf = {
		"glob_pattern" : "images/*.jpg",
		"thumbnail_path" : "thumbs/",
		"base_dimension" : 20,
		"downsample_dimension" : 30,
		"tile_base_size" : 225,
		"export_thumbnails" : True,
		"overwrite_thumbnails" : True,
		"colormode" : "HSV",
		"database" : "data",
		"table" : "mosaic_images",
		"start_tolerance" : 0.001,
		"tolerance_increment" : 0.0005
	}

	images = get_image_list(conf)
	colorList = []

	baseImagePath = "images/2015-10-22.jpg"
	baseImage = Image.open(baseImagePath)
	if conf["colormode"] == "L":
		baseImage = baseImage.convert("L")
	# baseImage = open_random_rgb_image(images, conf)
	resize_image_longside(baseImage, conf["base_dimension"])
	colorList = calculate_avg_colors(images, conf)
	# write_image_colors(colorList, conf)
	imagePaths = find_images(baseImage, colorList, conf)
	set_thumbnails(imagePaths, conf)
	svg = generate_svg(imagePaths, baseImage, conf)
	svg.write("mosaic.svg")